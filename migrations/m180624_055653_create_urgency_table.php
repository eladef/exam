<?php

use yii\db\Migration;

/**
 * Handles the creation of table `urgency`.
 */
class m180624_055653_create_urgency_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('urgency', [
            'id' => $this->primaryKey(),
                        'name'=> $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('urgency');
    }
}
