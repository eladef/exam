<?php

use yii\db\Migration;

/**
 * Class m180624_082228_init_rbac
 */
class m180624_082228_init_rbac extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
    {
        $auth = Yii::$app->authManager;
      $employee = $auth->createRole('employee');
      $auth->add($employee);

      $manager = $auth->createRole('manager');
      $auth->add($manager);
              
      $auth->addChild($manager, $employee);

      $updateProfile = $auth->createPermission('updateProfile');
      $auth->add($updateProfile);

      $createTask = $auth->createPermission('createTask');
      $auth->add($createTask);

      $viewTask = $auth->createPermission('viewTask');
      $auth->add($viewTask);

      $viewUser = $auth->createPermission('viewUser');
      $auth->add($viewUser);

      $editTask = $auth->createPermission('editTask');
      $auth->add($editTask);                    
              
      $updateOwnProfile = $auth->createPermission('updateOwnProfile');

      $rule = new \app\rbac\AuthorRule;
      $auth->add($rule);
              
      $updateOwnProfile->ruleName = $rule->name;                
      $auth->add($updateOwnProfile);                 
                                
              
      $auth->addChild($manager, $editTask);
      $auth->addChild($manager, $updateProfile);
      $auth->addChild($updateOwnProfile, $updateProfile);
      $auth->addChild($employee, $createTask);
      $auth->addChild($employee, $viewTask);
      $auth->addChild($employee, $viewUser);
      $auth->addChild($employee, $updateOwnProfile);
    }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180624_082228_init_rbac cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180624_082228_init_rbac cannot be reverted.\n";

        return false;
    }
    */
}
